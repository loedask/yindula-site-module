<?php

namespace Modules\YindulaSite\Tests\Feature;

use Tests\TestCase;
use Modules\YindulaCore\CommonConstant;
use Modules\YindulaCore\Constants\HttpStatusCodeConstant;

class SiteRoutesTest extends TestCase
{
    /**
     * Test the home page route.
     */
    public function testHomePageRoute()
    {
        $response = $this->get('/');

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
    }

    /**
     * Test the blog route.
     */
    public function testBlogRoute()
    {
        $response = $this->get('/blog');

        $response->assertStatus(200);
    }

    /**
     * Test the contact form submission route.
     */
    public function testContactFormSubmissionRoute()
    {
        $data = [
            'name' => CommonConstant::DEFAULT_FULLNAME,
            'email' => CommonConstant::DEFAULT_EMAIL,
            'message' => 'This is a test message.',
        ];

        $response = $this->post('/contact', $data);

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
    }

    /**
     * Test the slug route.
     */
    public function testSlugRoute()
    {
        $slug = 'page-title-slug';

        $response = $this->get('/' . $slug);

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
    }
}
