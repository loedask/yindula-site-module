<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\YindulaSite\app\Http\Controllers\ContactController;

$defaultPagesController = 'Modules\FrontPages\Http\Controllers\FrontPagesController';
$newFrontPagesController = 'Modules\FrontPages\app\Http\Controllers\FrontPagesController';

/*
|--------------------------------------------------------------------------
| Home Page Route
|--------------------------------------------------------------------------
|
| This is the route when a user visits a single page
|
*/
Route::get('/', [class_exists($newFrontPagesController) ? $newFrontPagesController : $defaultPagesController, 'site'])->name('homePage');

/*
|--------------------------------------------------------------------------
| Blog Routes
|--------------------------------------------------------------------------
|
| The following routes are the routes that handle the blog functionality
|
*/
Route::get('blog', 'PostController@index')->name('blog');
Route::get('blog/{slug}', 'PostController@show')->name('blog.post');


/*
|--------------------------------------------------------------------------
| Contact Routes
|--------------------------------------------------------------------------
|
| The following routes are the routes that handle the contact functionality
|
*/
Route::post('contact', [ContactController::class, 'submitContactForm'])->name('contact.submitContactForm');

/*
|--------------------------------------------------------------------------
| Pages Route
|--------------------------------------------------------------------------
|
| This is the route when a user visits a single page
|
*/
Route::get('{slug}', [class_exists($newFrontPagesController) ? $newFrontPagesController : $defaultPagesController, 'site'])->name('site');

/*
|--------------------------------------------------------------------------
| Language Routes
|--------------------------------------------------------------------------
|
| The following routes are the routes that handle the blog functionality
|
*/
Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('blog/locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});

