# Yindula Site
This module serves as a bridge or intermediary between the Yindula Cms module and the FrontPages module.

This FrontPages module facilitates the design aspect of the Websites/CMSs I develop. Since different websites require distinct designs, the FrontPages module seamlessly integrates with other Yindula modules.

## Prerequisites

- [Git](https://git-scm.com/downloads) installed.
- [Laravel Framework](https://laravel.com) installed.
- [Laravel Modules](https://github.com/nwidart/laravel-modules) installed.
- [Laravel Module Installer](https://github.com/joshbrw/laravel-module-installer) installed.

### Important Notice

1. Before proceeding, ensure that you have installed the "[nwidart/laravel-modules](https://github.com/nwidart/laravel-modules)" package in your Laravel project.
Follow the provided instructions to install this package, as it allows for module installation within Laravel projects.

2. Once the "nwidart/laravel-modules" package is installed, an additional step is required to install modules into the "Modules" directory of your project.

3. To facilitate the automatic movement of Yindula Site module files, you need to install the "[joshbrw/laravel-module-installer](https://github.com/joshbrw/laravel-module-installer)" composer plugin.

To install the plugin, open a terminal in the root directory of your Laravel project and execute the following command:

   ```bash
    composer require joshbrw/laravel-module-installer
   ```

4. After successfully installing the "joshbrw/laravel-module-installer" plugin, you can proceed with the installation of the Yindula Site module.
During the installation process, the Yindula Site module will be automatically placed in the "Modules" directory of your project.

Refer to the "Installation and Build" section for the appropriate command or instructions to install a specific module.
The "joshbrw/laravel-module-installer" plugin will handle the automatic movement of Yindula Site module files to the "Modules" directory.

## Installation and Build

To install Yindula Site for your application, follow these steps:

1. Open a terminal in the root directory of your application.

2. Use the following command to install Yindula Site:

   ```bash
   composer require loecoscorp/yindula-site-module
   ```

This command will download and install the Yindula Site module for your application.

Once the installation is complete, you can proceed with the configuration and usage of Yindula Site in your project.
