<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert(array (
            0 => 
            array (
                'color' => '#000000',
                'created_at' => '2020-03-21 23:05:57',
                'icon_class' => NULL,
                'id' => 22,
                'menu_id' => 2,
                'order' => 1,
                'parameters' => NULL,
                'parent_id' => NULL,
                'route' => NULL,
                'target' => '_self',
                'title' => 'Home',
                'updated_at' => '2020-03-26 00:10:18',
                'url' => '/',
            ),
            1 => 
            array (
                'color' => '#000000',
                'created_at' => '2020-03-21 23:06:20',
                'icon_class' => NULL,
                'id' => 23,
                'menu_id' => 2,
                'order' => 2,
                'parameters' => NULL,
                'parent_id' => NULL,
                'route' => NULL,
                'target' => '_self',
                'title' => 'Who We Are',
                'updated_at' => '2023-04-22 09:54:16',
                'url' => 'we-are-the-loecos-corporate',
            ),
            2 => 
            array (
                'color' => '#000000',
                'created_at' => '2021-07-06 02:20:31',
                'icon_class' => NULL,
                'id' => 33,
                'menu_id' => 2,
                'order' => 7,
                'parameters' => NULL,
                'parent_id' => NULL,
                'route' => NULL,
                'target' => '_self',
                'title' => 'Some of Our Work',
                'updated_at' => '2022-12-28 09:17:52',
                'url' => 'some-of-our-work',
            ),
        ));
        
        
    }
}