<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('features')->delete();

        \DB::table('features')->insert(array (
            0 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2020-03-09 02:10:25',
                'description' => '<p><a class="badge badge-primary">Fully customized</a><br><a class="badge badge-primary">Single page</a><br><a class="badge badge-primary">Realtime</a><br><a class="badge badge-primary">Responsive</a></p>',
                'excerpt' => NULL,
                'feature_section_id' => 1,
                'icon' => 'icon-cogs',
                'id' => 1,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => 1,
                'title' => 'Web development',
                'updated_at' => '2022-12-27 13:49:07',
                'url' => NULL,
            ),
            1 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2020-03-11 11:09:26',
                'description' => '<p><a class="badge badge-primary">Native</a><br><a class="badge badge-primary">Cross-platform</a><br><a class="badge badge-primary">Multi-device</a><br><a class="badge badge-primary">Support</a></p>',
                'excerpt' => NULL,
                'feature_section_id' => 1,
                'icon' => 'icon-cogs',
                'id' => 2,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => 1,
                'title' => 'Mobile application',
                'updated_at' => '2022-12-27 15:02:58',
                'url' => NULL,
            ),
            2 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2020-03-11 11:16:20',
                'description' => '<p><a class="badge badge-primary">Web application</a><br><a class="badge badge-primary">Mobile application</a><br><a class="badge badge-primary">Graphic design</a><br><a class="badge badge-primary">Brand identity</a></p>',
                'excerpt' => NULL,
                'feature_section_id' => 1,
                'icon' => 'icon-cogs',
                'id' => 3,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => 1,
                'title' => 'Design',
                'updated_at' => '2022-12-27 15:12:36',
                'url' => NULL,
            ),
            3 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2020-03-28 16:26:34',
                'description' => NULL,
                'excerpt' => NULL,
                'feature_section_id' => 3,
                'icon' => NULL,
                'id' => 4,
                'image' => 'features\\December2022\\DKim93kDl8bJkDyDSHkh.png',
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => 1,
                'title' => 'Development Life Cycle',
                'updated_at' => '2022-12-28 09:38:08',
                'url' => NULL,
            ),
            4 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-09-11 08:15:14',
                'description' => NULL,
                'excerpt' => NULL,
                'feature_section_id' => 4,
                'icon' => NULL,
                'id' => 5,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Analysis',
                'updated_at' => '2022-09-11 08:16:34',
                'url' => NULL,
            ),
            5 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-09-11 11:51:09',
                'description' => NULL,
                'excerpt' => NULL,
                'feature_section_id' => 4,
                'icon' => NULL,
                'id' => 6,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Implementation',
                'updated_at' => '2022-09-11 11:52:59',
                'url' => NULL,
            ),
            6 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-09-11 11:52:08',
                'description' => NULL,
                'excerpt' => NULL,
                'feature_section_id' => 4,
                'icon' => NULL,
                'id' => 7,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Integration',
                'updated_at' => '2022-09-11 11:53:43',
                'url' => NULL,
            ),
            7 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-09-11 12:17:56',
                'description' => '<p><a class="badge badge-primary">Analysis </a><br><a class="badge badge-primary">Implementation </a><br><a class="badge badge-primary">Integration </a><br><a class="badge badge-primary">Maintenance and Support</a></p>',
                'excerpt' => NULL,
                'feature_section_id' => 1,
                'icon' => NULL,
                'id' => 8,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Automation',
                'updated_at' => '2022-12-28 08:29:11',
                'url' => NULL,
            ),
            8 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-12-28 09:27:26',
                'description' => '<p>We are excited about collaborating with you to develop a product that will propel your business to new heights.</p>',
                'excerpt' => NULL,
                'feature_section_id' => 6,
                'icon' => NULL,
                'id' => 9,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Are you ready to collaborate with us?',
                'updated_at' => '2023-04-22 09:37:14',
                'url' => NULL,
            ),
            9 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-12-28 09:59:54',
                'description' => '<p>Cicole is an Educational Management Solution developed to address the administrative, curriculum, staff, and financial functions and controls required by schools.</p>',
                'excerpt' => NULL,
                'feature_section_id' => 7,
                'icon' => 'fa-globe',
                'id' => 10,
                'image' => 'features\\December2022\\tCtiCvtBwIsTpIHFCijh.png',
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Cicole Web App',
                'updated_at' => '2023-04-22 12:20:29',
                'url' => 'https://app.cicole.net',
            ),
            10 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-12-28 10:00:22',
                'description' => '<p>This is the mobile version of the Cicole App designed for parents to conveniently track their child\'s or children\'s progress at school.</p>',
                'excerpt' => NULL,
                'feature_section_id' => 7,
                'icon' => 'fa-mobile',
                'id' => 11,
                'image' => 'features\\December2022\\2vYzBOMYfakWVa7CGqqF.png',
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Cicole Mobile App',
                'updated_at' => '2023-04-22 12:18:04',
                'url' => 'https://play.google.com/store/apps/details?id=cicole.net',
            ),
            11 =>
            array (
                'category_id' => NULL,
                'content' => NULL,
                'created_at' => '2022-12-28 10:00:48',
                'description' => '<p>Kutala Desktop is a powerful software that turns your computer and display/projector into an interactive sports scoreboard, ideal for engaging spectators during basketball, volleyball, hockey, and a variety of other sports events.</p>
<p>Contact us at hello@loecos.net to schedule a demo of our software.</p>',
                'excerpt' => NULL,
                'feature_section_id' => 7,
                'icon' => 'fa-desktop',
                'id' => 12,
                'image' => NULL,
                'is_active' => 1,
                'page_id' => 1,
                'sort_order' => NULL,
                'title' => 'Kutala Desktop',
                'updated_at' => '2023-04-22 12:17:20',
                'url' => NULL,
            ),
        ));


    }
}
