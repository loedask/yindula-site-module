<?php

namespace Modules\YindulaSite\database\seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\YindulaCore\Database\Seeders\DefaultUserTableSeeder;

class YindulaSiteDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Model::unguard();

        $this->call(CmsSettingsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(FeatureSectionsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);

        $this->call(DefaultUserTableSeeder::class);
        $this->call(CmsEventsTableSeeder::class);
    }
}
