<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class FeatureSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('feature_sections')->delete();
        
        \DB::table('feature_sections')->insert(array (
            0 => 
            array (
                'category_id' => NULL,
                'created_at' => '2020-03-09 01:48:54',
                'description' => '<p>At the forefront of our priorities is ensuring our customers\' satisfaction. By carefully examining user requirements, preferences, and constraints, we leverage state-of-the-art technologies to develop high-performance, user-friendly mobile applications and responsive web designs at scale.</p>',
                'id' => 1,
                'image' => NULL,
                'is_active' => 1,
                'key' => 'services',
                'name' => 'Services',
                'page_id' => 2,
                'title' => 'Services',
                'updated_at' => '2023-04-22 09:31:11',
            ),
            1 => 
            array (
                'category_id' => NULL,
                'created_at' => '2020-03-21 23:35:52',
                'description' => '<p>Latest and emerging technologies are always preferred. Here is the brief of services that we are providing as well as a short list of technologies accompanied by the services.</p>',
                'id' => 2,
                'image' => NULL,
                'is_active' => 1,
                'key' => 'we-love-new-technologies',
                'name' => 'We love new technologies',
                'page_id' => 2,
                'title' => NULL,
                'updated_at' => '2022-09-11 04:05:34',
            ),
            2 => 
            array (
                'category_id' => NULL,
                'created_at' => '2020-03-26 22:01:02',
                'description' => '<p>Development Life Cycle</p>',
                'id' => 3,
                'image' => NULL,
                'is_active' => 1,
                'key' => 'development-process',
                'name' => 'Development Process',
                'page_id' => 2,
                'title' => NULL,
                'updated_at' => '2022-09-11 04:06:48',
            ),
            3 => 
            array (
                'category_id' => NULL,
                'created_at' => '2022-09-11 08:03:13',
                'description' => '<p>Our experts can give several recommendations for how to get started.&nbsp;</p>',
                'id' => 4,
                'image' => NULL,
                'is_active' => 0,
                'key' => 'business-processes-automation',
                'name' => 'Business Processes Automation',
                'page_id' => 2,
                'title' => 'How Can You Improve Your Business Through Process Automation ?',
                'updated_at' => '2022-09-11 08:03:13',
            ),
            4 => 
            array (
                'category_id' => NULL,
                'created_at' => '2022-11-05 12:06:46',
                'description' => '<p>Ratione voluptatum e</p>',
                'id' => 5,
                'image' => 'feature_sections\\November2022\\XAwhcYy0hSC2pJEwPkmI.jpg',
                'is_active' => 1,
                'key' => 'norman-ewing',
                'name' => 'Norman Ewing',
                'page_id' => 3,
                'title' => 'Reprehenderit aliqu',
                'updated_at' => '2022-11-05 12:06:46',
            ),
            5 => 
            array (
                'category_id' => NULL,
                'created_at' => '2022-12-28 09:23:55',
                'description' => NULL,
                'id' => 6,
                'image' => NULL,
                'is_active' => 1,
                'key' => 'ready-to-partner-with-us',
                'name' => 'Ready to partner with us',
                'page_id' => 2,
                'title' => 'Are you ready to collaborate with us?',
                'updated_at' => '2023-04-22 09:33:35',
            ),
            6 => 
            array (
                'category_id' => NULL,
                'created_at' => '2022-12-28 09:58:49',
                'description' => NULL,
                'id' => 7,
                'image' => NULL,
                'is_active' => 0,
                'key' => 'portfolio',
                'name' => 'Portfolio',
                'page_id' => 4,
                'title' => 'Portfolio',
                'updated_at' => '2022-12-28 09:58:49',
            ),
        ));
        
        
    }
}