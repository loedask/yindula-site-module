<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class CmsSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cms_settings')->delete();
        
        \DB::table('cms_settings')->insert(array (
            0 => 
            array (
                'created_at' => NULL,
                'id' => 1,
                'key' => 'name',
                'sort_order' => 1,
                'type' => 'app',
                'updated_at' => NULL,
                'value' => 'Yindula',
            ),
            1 => 
            array (
                'created_at' => NULL,
                'id' => 2,
                'key' => 'name',
                'sort_order' => 1,
                'type' => 'site',
                'updated_at' => NULL,
                'value' => 'LoecosCorp',
            ),
            2 => 
            array (
                'created_at' => '2023-04-22 10:48:55',
                'id' => 3,
                'key' => 'has_categories',
                'sort_order' => NULL,
                'type' => 'cms',
                'updated_at' => '2023-04-22 10:58:16',
                'value' => 'no',
            ),
            3 => 
            array (
                'created_at' => '2023-04-22 10:59:05',
                'id' => 4,
                'key' => 'has_posts',
                'sort_order' => NULL,
                'type' => 'cms',
                'updated_at' => '2023-04-22 10:59:05',
                'value' => 'no',
            ),
            4 => 
            array (
                'created_at' => '2023-04-22 11:03:17',
                'id' => 5,
                'key' => 'has_banners',
                'sort_order' => NULL,
                'type' => 'cms',
                'updated_at' => '2023-04-22 11:03:17',
                'value' => 'yes',
            ),
            5 => 
            array (
                'created_at' => '2023-04-22 11:57:43',
                'id' => 6,
                'key' => 'has_feature_sections',
                'sort_order' => NULL,
                'type' => 'cms',
                'updated_at' => '2023-04-22 11:58:42',
                'value' => 'yes',
            ),
            6 => 
            array (
                'created_at' => '2023-04-22 11:58:05',
                'id' => 7,
                'key' => 'has_features',
                'sort_order' => NULL,
                'type' => 'cms',
                'updated_at' => '2023-04-22 11:58:56',
                'value' => 'yes',
            ),
            7 => 
            array (
                'created_at' => '2023-07-09 06:53:14',
                'id' => 8,
                'key' => 'has_events',
                'sort_order' => 2,
                'type' => 'cms',
                'updated_at' => '2023-10-07 09:43:01',
                'value' => 'no',
            ),
            8 =>
            array (
                'created_at' => '2023-07-09 06:53:14',
                'id' => 9,
                'key' => 'has_sliders',
                'sort_order' => 2,
                'type' => 'cms',
                'updated_at' => '2023-10-07 09:43:01',
                'value' => 'no',
            ),
        ));


    }
}
