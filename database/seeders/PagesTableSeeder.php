<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('pages')->delete();

        \DB::table('pages')->insert(array (
            0 =>
            array (
                'author_id' => 1,
                'body' => '<p>Loecos Corporate is a dynamic tech company founded by a team of passionate individuals who love what they do.</p>
<p>Our innovative and energetic team is continuously expanding as we build exceptional applications, provide practical solutions, develop bespoke tools tailored to meet organizations\' unique needs, and grow our community.</p>',
                'created_at' => '2020-02-08 06:40:21',
                'excerpt' => '<p>Let us have the honor of turning your vision into reality by bringing your project to life.</p>',
                'id' => 1,
                'image' => 'pages\\October2023\\HMV2vm5KELH5TSFicB7o.jpg',
                'is_contact' => 0,
                'is_event' => 0,
                'is_home' => 0,
                'is_page' => 1,
                'meta_description' => 'Yar Meta Description',
                'meta_keywords' => NULL,
                'slug' => 'we-are-the-loecos-corporate',
                'status' => 'ACTIVE',
                'subtitle' => NULL,
                'title' => 'We are the Loecos Corporate',
                'updated_at' => '2023-10-07 10:03:53',
            ),
            1 =>
            array (
                'author_id' => 1,
                'body' => '<p>This is the one</p>',
                'created_at' => '2022-09-10 19:24:23',
                'excerpt' => NULL,
                'id' => 2,
                'image' => NULL,
                'is_contact' => 0,
                'is_event' => 0,
                'is_home' => 1,
                'is_page' => 0,
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'slug' => 'home',
                'status' => 'ACTIVE',
                'subtitle' => NULL,
                'title' => 'Home',
                'updated_at' => '2023-10-07 09:32:03',
            ),
            2 =>
            array (
                'author_id' => 1,
                'body' => NULL,
                'created_at' => '2022-09-10 19:24:51',
                'excerpt' => NULL,
                'id' => 3,
                'image' => NULL,
                'is_contact' => 1,
                'is_event' => 0,
                'is_home' => 0,
                'is_page' => 0,
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'slug' => 'contact-us',
                'status' => 'ACTIVE',
                'subtitle' => NULL,
                'title' => 'Contact Us',
                'updated_at' => '2023-10-07 09:28:14',
            ),
            3 =>
            array (
                'author_id' => 1,
                'body' => NULL,
                'created_at' => '2022-12-28 09:17:20',
                'excerpt' => NULL,
                'id' => 4,
                'image' => NULL,
                'is_contact' => 0,
                'is_event' => 0,
                'is_home' => 0,
                'is_page' => 0,
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'slug' => 'some-of-our-work',
                'status' => 'ACTIVE',
                'subtitle' => NULL,
                'title' => 'Some of Our Work',
                'updated_at' => '2023-10-07 09:28:14',
            ),
        ));


    }
}
