<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'created_at' => '2020-03-08 19:19:12',
                'id' => 2,
                'name' => 'main',
                'updated_at' => '2020-03-08 19:19:12',
            ),
        ));
        
        
    }
}