<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banners')->delete();
        
        \DB::table('banners')->insert(array (
            0 => 
            array (
                'created_at' => '2020-02-09 11:03:00',
                'description' => '<p>We create innovative solutions for exceptional individuals.</p>',
                'excerpt' => NULL,
                'id' => 1,
                'image' => 'banners/January2021/7dmgpnc2HW0Cm7ooY1AZ.png',
                'image_two' => NULL,
                'is_default' => 1,
                'title' => 'How can we assist you with your business?',
                'updated_at' => '2023-04-22 09:27:37',
                'video' => NULL,
            ),
        ));
        
        
    }
}