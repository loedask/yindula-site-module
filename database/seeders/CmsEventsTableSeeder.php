<?php

namespace Modules\YindulaSite\Database\Seeders;

use Illuminate\Database\Seeder;

class CmsEventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cms_events')->delete();
        
        \DB::table('cms_events')->insert(array (
            0 => 
            array (
                'created_at' => '2023-07-09 08:30:13',
                'description' => 'Tempora officiis eius molestias dolor accusamus enim fugit natus pariatur Molestiae recusandae Consequat Id odio commodi sit omnis doloremque nemo',
                'end_date' => '2023-07-09',
                'end_time' => '17:23:00',
                'id' => 1,
                'image' => 'cms_events\\July2023\\45ri1FBQ2ISTIf4kgDZV.jpg',
                'is_full_day' => 1,
                'start_date' => '2023-07-09',
                'start_time' => '17:56:00',
                'title' => 'Veritatis sed praesentium',
                'updated_at' => '2023-07-09 08:30:13',
            ),
            1 => 
            array (
                'created_at' => '2023-07-09 08:36:07',
                'description' => 'Eum aliquip quisquam quo nesciunt eum consectetur qui voluptas necessitatibus harum',
                'end_date' => '2023-07-10',
                'end_time' => '03:51:00',
                'id' => 2,
                'image' => 'cms_events\\July2023\\45ri1FBQ2ISTIf4kgDZV.jpg',
                'is_full_day' => 1,
                'start_date' => '2023-07-10',
                'start_time' => '05:29:00',
                'title' => 'Architecto ex beatae qui optio',
                'updated_at' => '2023-07-09 08:36:07',
            ),
        ));
        
        
    }
}