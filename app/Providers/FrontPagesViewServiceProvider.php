<?php

namespace Modules\YindulaSite\app\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use Modules\YindulaCms\app\Repositories\PostRepository;
use Modules\YindulaCms\app\Repositories\BannerRepository;
use Modules\YindulaCms\app\Repositories\SliderRepository;
use Modules\YindulaCms\app\Repositories\FeatureRepository;
use Modules\YindulaCms\app\Repositories\GalleryRepository;
use Modules\YindulaCms\app\Repositories\CmsEventRepository;
use Modules\YindulaCms\app\Repositories\FeatureSectionRepository;

class FrontPagesViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $feature = app(FeatureRepository::class);
        $feature_section = app(FeatureSectionRepository::class);

        $gallery = app(GalleryRepository::class);
        $post = app(PostRepository::class);
        $slider = app(SliderRepository::class);
        $cms_event = app(CmsEventRepository::class);

        $viewShare['feature'] = $feature;
        $viewShare['feature_section'] = $feature_section;
        $viewShare['gallery'] = $gallery;
        $viewShare['post'] = $post;
        $viewShare['slider'] = $slider;
        $viewShare['cms_event'] = $cms_event;

        view()->share($viewShare);

        View::composer(['frontpages::banners.index'], function ($view) {
            $banners = app(BannerRepository::class)->fetchAll();
            $view->with('banners', $banners);
        });

        View::composer(['frontpages::sliders.index'], function ($view) {
            $sliders = app(SliderRepository::class)->fetchAll();
            $view->with('sliders', $sliders);
        });

        View::composer(['frontpages::cms_events.index'], function ($view) {
            $cms_events = app(CmsEventRepository::class)->fetchAll();
            $view->with('cms_events', $cms_events);
        });
    }
}
