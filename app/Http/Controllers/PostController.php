<?php

namespace Modules\YindulaSite\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\YindulaCms\app\Models\Post;
use Modules\YindulaCms\app\Repositories\PostRepository;

class PostController extends Controller
{

    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
    }

    public function index()
    {
        $posts = (new Post())->getPosts();

        $seo = (object)array(
            'title' => setting('title') . ' Blog',
            'meta_description' => setting('title') . ' Blog'
        );

        $headline = 'Welcome to the ' . setting('title') . ' Blog';

        return view('frontend::post.index', compact('posts', 'headline', 'seo'));
    }

    public function show($slug)
    {
        $single_post = $this->postRepository->findBySlug($slug);
        // $random_posts = $this->postRepository->findRandomPosts($single_post);

        $headline = $single_post->title;

        // $seo = (object)array(
        //     'title' => $post->getTranslatedAttribute('title'),
        //     'meta_description' => $post->getTranslatedAttribute('meta_description'),
        //     'meta_keywords' => $post->getTranslatedAttribute('meta_keywords'),
        //     'twitter_description' => $post->getTranslatedAttribute('meta_description'),
        //     'og_title' => $post->getTranslatedAttribute('title'),
        //     'og_image' => Voyager::image($post->image),
        // );

        $seo = (object)array(
            'title' => $single_post->title,
            'seo_title' => $single_post->seo_title,
            'meta_description' => $single_post->meta_description,
            'meta_keywords' => $single_post->meta_keywords,
            'twitter_description' => $single_post->meta_description,
            // 'og_title' => $post->title,
            // 'og_image' => Voyager::image($post->image),
        );

        return view('frontpages::posts.show', compact(
            'single_post',
            // 'random_posts',
            'headline',
            'seo'
        ));
    }
}
