<?php

namespace Modules\YindulaSite\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Modules\YindulaSite\Mail\ContactFormMail;

class ContactController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submitContactForm(Request $request)
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        Mail::to('hello@loecos.net')->send(new ContactFormMail($data));

        return redirect()->back()->withSuccess('Thanks for your message. We\'ll be in touch.');
    }
}
