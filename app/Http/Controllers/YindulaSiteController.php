<?php

namespace Modules\YindulaSite\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\YindulaCms\app\Models\Page;
use Modules\YindulaCms\app\Repositories\PageRepository;

class YindulaSiteController extends Controller
{

    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }


    /**
     * Retrieve the requested page.
     *
     * @param string $slug the slug of the page to retrieve
     * @return Page the requested page
     */
    private function getPage($slug)
    {
        // Retrieve the home page if the slug parameter is empty.
        if (empty($slug)) {
            return $this->pageRepository->findHomePage();
        }

        // Find the page based on the provided slug.
        $page = $this->pageRepository->findBySlug($slug);


        // If the data retrieval process fails,
        // we will display the default 404 error page.
        if (empty($page)) {
            abort(404, 'Page not found.');
        }

        return $page;
    }


    /**
     * Get the SEO data for a page.
     *
     * @param  string  $title
     * @param  string  $description
     *
     * @return object
     */
    private function getSeo($title, $description)
    {
        return (object) [
            'title' => $title,
            'meta_description' => $description,
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function site($slug = null)
    {
        $page = $this->getPage($slug);

        // Check if $page is null before accessing its properties
        if ($page === null) {
            abort(404, 'Page not found.');
        }

        $is_home = $this->pageRepository->isHome($page->slug);

        $is_contact = $this->pageRepository->isContact($page->slug);

        $is_event = $this->pageRepository->isEvent($page->slug);

        $section =  $page->featureSection->pluck('key')->toArray();
        $section = collect($section);

        $headline = $page->title;

        $seo = $this->getSeo($page->title, $page->title . ' Page');

        return view('frontpages::site', compact(
            'page',
            'is_home',
            'is_contact',
            'is_event',
            'headline',
            'seo',
            'section',
        ));
    }
}
