<?php

namespace Modules\YindulaSite\app\Console\Commands;

use Illuminate\Console\Command;

class SeedSiteDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:seed-site-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed Site module with dummy data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Ask the user if they want to seed dummy data
        if ($this->confirm('Do you want to seed the Site with dummy data?')) {

            // Ask the user if they want to refresh the database
            if ($this->confirm('Do you want to refresh the database before seeding?')) {

                // Run the migration
                $this->info('--- Refreshing the database ---');
                $this->call('migrate:refresh');

                // Run the module seed command
                $this->info('--- Running the YindulaSite seed command ---');
                $this->call('module:seed', ['module' => 'YindulaSite']);

                $this->info('YindulaSite module seeded successfully.');
            } else {
                // Run the migration without refreshing the database
                // if ($this->confirm('Do you want to run the migration without refreshing the database?')) {
                //     $this->info('--- Running the migration ---');
                //     $this->call('migrate');
                // } else {
                //     $this->info('Migration skipped.');
                // }

                // Run the migration
                $this->info('--- Running the migration ---');
                $this->call('migrate');

                // Run the module seed command
                $this->info('--- Running the YindulaSite seed command ---');
                $this->call('module:seed', ['module' => 'YindulaSite']);

                // $this->info('Seed operation cancelled.');
                $this->info('YindulaSite module seeded successfully.');
            }

        } else {
            $this->info('Seed operation cancelled.');
        }
    }
}
