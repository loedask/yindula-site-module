<?php

namespace Modules\YindulaSite\app\Console\Traits;


use Modules\YindulaSite\app\Console\Commands\SeedSiteDataCommand;


trait RegistersCommandsTrait
{
    /**
     * Register commands in the format of Command::class
     */
    protected function registerCommands(): void
    {
        $this->commands([
            SeedSiteDataCommand::class,
        ]);
    }
}
